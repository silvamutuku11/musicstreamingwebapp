@extends('layouts.app')

@section('content')
    <section class="fullwidth-block inner-content">
        <div class="container">
            <div class="row">

                <div class="col-md-7">
                    <div class="content">
    <h2 class="entry-title">About MwendoTaji</h2>

    <h3 class="text-white">The beginning…</h3>

    <p class="text-justify">Our Story is an inspiring one, It starts back in the Year 2005 when Alfa, the founder of Taji Music came up with the Idea.......
        to I.O.YOU.</p>
    <figure>
        <img src="{{asset('images/About/about-1.jpg')}}" class="figure-img img-fluid rounded mx-auto d-block">
        <figcaption class="figure-caption text-center">Mark, Kian and Shane in I.O.YOU (also pictured are Derek, Graham and
            Michael)</figcaption>
    </figure>

                    </div>
                </div>

                @include('discography')
            </div>
        </div>
    </section>
@endsection
