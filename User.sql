/*
SQLyog Ultimate v11.5 (64 bit)
MySQL - 10.1.38-MariaDB : Database - bestlaravel2018
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bestlaravel2018` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `bestlaravel2018`;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`admin`,`remember_token`,`created_at`,`updated_at`,`address`,`city`,`state`,`country`,`pincode`,`mobile`) values (1,'WeShare','demo@gmail.com',NULL,'$2y$10$m9fNpTgwyBVqqVfsJ9bXUensvx5iqlYhzqmL3khhSpKpgqNQnW0t2',1,'fWKNx9xmTylpd5OCPkLXBNWlkXNdRHuDVnaJY97cbW3WIMgGD8lg5xsUxSR3','2018-10-15 05:32:54','2018-12-05 04:39:52','123 Street','Phnom Penh','PP','Cambodia','12252','010313234'),(4,'weshare','weshare@gmail.com',NULL,'$2y$10$3Ccxg17LYw/.qS7ib5Xcr.T5po6AXUsnjEcEI4IHcQ0MGkcuRfO.O',NULL,'za7FtmzYvfzBYmkQtE5tfvStl7dY3Z6uZKSpuRtBRIvlbXzM0csZEQYzjuEb','2018-12-06 04:40:27','2018-12-06 04:40:27',NULL,NULL,NULL,NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
