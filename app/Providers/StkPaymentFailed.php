<?php

namespace App\Providers;

use App\Events\Samerior\MobileMoney\Mpesa\Events\StkPushPaymentFailedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class StkPaymentFailed
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StkPushPaymentFailedEvent  $event
     * @return void
     */
    public function handle(StkPushPaymentFailedEvent $event)
    {
        //
    }
}
