<?php

namespace App\Jobs;

use App\Album;
use App\Providers\StkPaymentReceived;
use Composer\Downloader\DownloaderInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessDownload implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $album;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Album $album)
    {
        $this->download = $album;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(StkPaymentReceived $payment)
    {
        //
    }
}
