<?php

namespace App\Http\Middleware;

use App\Album;
use Closure;
use Illuminate\Support\Facades\Auth;

class hasPaid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $paid = Album::all();
        dd($paid);
        // if()
        // if (Auth::user()) {
        //     if (Auth::user()->admin() == true) {
        //         return $next($request);
        //     }
        //     return redirect('/album')->with('fail', 'You have not paid for this album');
        // }
        // return redirect('/album')->with('adminFailLogin', 'You are not administrator');
        return redirect('/album')->with('fail', 'You have not paid for this album');
    }
}
