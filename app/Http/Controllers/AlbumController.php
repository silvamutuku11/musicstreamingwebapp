<?php

namespace App\Http\Controllers;

use App\Album;
use App\Gallery;
use App\User;
use Illuminate\Http\Request;
use Samerior\MobileMoney\Mpesa\Facades\STK;
use App\Http\Controllers\Redirector\view;
use ZipArchive;

class AlbumController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }

  /*
   * ////////////////////////////////////////////////////////////////////
   *
   * FOR ADMIN PAGE
   *
   * ///////////////////////////////////////////////////////////////////
   */

  public function adminIndex()
  {
    $albums = Album::all();

    return view('admin/album', compact('albums'));
  }


  public function view(Album $album)
  {
    $album = Album::find($album)->first();

    return view('admin/album_view', compact('album'));
  }


  public function create()
  {
    $gallery = Gallery::where('name', '=', 'Band')->first();
    return view('admin/album_form', compact('gallery'));
  }


  public function store(Request $request)
  {
    $path = 'Album/';

    $this->validate($request, [
      'title' => 'required|string|unique:albums',
      'released' => 'required',
      'note' => 'nullable|string',
      'summary' => 'required|string',
      'logo' => 'required'
    ]);

    $logo = $request->file('logo');
    $logoExt = $logo->clientExtension();
    $logoName = $request->title . "." . $logoExt;


    $logo->move('images/Album', $logoName);

    $newCover = $request->file('newCover');
    $defaultCover = $request->defaultCover;

    if ($defaultCover == null && $newCover != null) {
      $coverExt = $newCover->clientExtension();
      $coverImage =  $request->title . ' Cover' . "." . $coverExt;
      $newCover->move('images/Album', $coverImage);
    } else if ($newCover == null && $defaultCover != null) {
      $coverImage = $defaultCover;
    } else {
      $coverImage = 'westlife-2.jpg';
    }

    Album::create([
      'title' => $request->title,
      'logo' => $path . $logoName,
      'summary' => $request->summary,
      'released' => $request->released,
      'cover' => $path . $coverImage,
      'note' => $request->note
    ]);

    return redirect('/admin/album');
  }

  public function edit(Album $album)
  {
    $gallery = Gallery::where('name', '=', 'Band')->first();
    return view('admin/album_edit', compact('album', 'gallery'));
  }


  public function update(Request $request, Album $album)
  {
    $path = 'Album/';
    $newLogo = $request->file('logo');
    $newCover = $request->file('newCover');
    $defaultCover = $request->defaultCover;

    if ($request->title == null) {
      $this->validate($request, [
        'title' => 'nullable|string|unique:albums',
        'released' => 'required',
        'note' => 'nullable|string',
        'summary' => 'required|string'
      ]);
      $albumTitle = $album->title;
    } else {
      $this->validate($request, [
        'title' => 'required|string|unique:albums',
        'released' => 'required',
        'note' => 'nullable|string',
        'summary' => 'required|string'
      ]);
      $albumTitle = $request->title;
    }

    if ($newLogo == null) {
      $updateLogo = $album->logo;
    } else {
      $updateLogoExt = $newLogo->clientExtension();
      $updateLogo = $path . $request->title . "." . $updateLogoExt;
      $newLogo->move('images/Album', $updateLogo);
    }

    if ($newCover == null && $defaultCover == 'none') {
      $updateCover = $album->cover;
    } else if ($newCover != null && $defaultCover == null) {
      $updateCoverExt = $newCover->clientExtension();
      $updateCover =  $path . $request->title . ' Cover' . "." . $updateCoverExt;
      $newCover->move('images/Album', $updateCover);
    } else if ($newCover == null && $defaultCover) {
      $updateCover = $path . $defaultCover;
    }

    $album->update([
      'title' => $albumTitle,
      'logo' => $updateLogo,
      'summary' => $request->summary,
      'released' => $request->released,
      'cover' => $updateCover,
      'note' => $request->note
    ]);

    return redirect('/admin/album/view/' . $album->id)->with('UpdateAlbum', 'Update album successfully');
  }


  public function destroy(Album $album)
  {
    try {
      $album->delete();
      return redirect('/admin/album')->with('DeleteAlbum', 'Delete Album successfully');
    } catch (\Exception $e) {
      $e->getMessage();
    }
  }


  /*
   * ////////////////////////////////////////////////////////////////////
   *
   * FOR USER PAGE
   *
   * ///////////////////////////////////////////////////////////////////
   */

  public function index()
  {
    $albums = Album::all();

    return view('album/index', compact('albums'));
  }

  public function show($albumName)
  {

    $albumNa = preg_replace('/\-/', ' ', $albumName);
    $album = Album::where('title', '=', $albumNa)->first();

    foreach ($album->songs as $song) {
      $file = new mp3Controller("storage/albums/" . $albumNa . "/" . $song->url);
      $duration = $file->getDuration();
      $length = mp3Controller::formatTime($duration);
      $song['length'] = $length;
    }

    return view('album/show', compact('album'));
  }

  public function listen($albumName)
  {
    $albumNa = preg_replace('/\-/', ' ', $albumName);
    $albumListen = Album::where('title', '=', $albumNa)->first();

    foreach ($albumListen->songs as $song) {
      $file = new mp3Controller("storage/albums/" . $albumNa . "/" . $song->url);
      $duration = $file->getDuration();
      $length = mp3Controller::formatTime($duration);
      $song['length'] = $length;
    }

    return view('album/listen', compact('albumListen'));
  }

  public function download($albumName)
  {
    // $this->middleware('auth');
    $albumNa = preg_replace('/\-/', ' ', $albumName);
    $album = Album::where('title', '=', $albumNa)->first();
    $customerDetail = \Auth::user();
    $customerPhone = $customerDetail->phoneNumber;
    $summary = $album->summary;

    $BusinessShortCode = '174379';
    $LipaNaMpesaPasskey = 'bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919';
    $TransactionType = 'CustomerPayBillOnline';
    $Amount = '1';
    $PartyA = $customerPhone;
    $PartyB = '174379';
    $PhoneNumber = '254741405470';
    $CallBackURL = 'https://0e4f5835.ngrok.io';
    $AccountReference = 'TajiMusic';
    $TransactionDesc = 'download Album' . $albumNa;
    $Remarks = $summary;

    $mpesa = new \Safaricom\Mpesa\Mpesa();

    $stkPushSimulation = $mpesa->STKPushSimulation($BusinessShortCode, $LipaNaMpesaPasskey, $TransactionType, $Amount, $PartyA, $PartyB, $PhoneNumber, $CallBackURL, $AccountReference, $TransactionDesc, $Remarks);

    // var_dump($stkPushSimulation); die();
    if ($stkPushSimulation == '{"Message":"invalid application status"}') {
      // dd($stkPushSimulation);
      $albumNa = preg_replace('/\-/', ' ', $albumName);
      $songsDownload = array();
      return redirect()->route('/storage/albums/' . $albumName);
      return redirect()->view('album/show', compact('album'));
    } else {

      $callbackData = $mpesa->getDataFromCallback();
      // dd($callbackData);
      $stkPushSimulation = json_decode($stkPushSimulation);
      $stkPushSimulation = (array) $stkPushSimulation;
      if (isset($stkPushSimulation['ResponseCode'])) {

        $response = $stkPushSimulation['ResponseCode']; //->ResponseCode;
      } else {
        $response = $stkPushSimulation['errorMessage'];
      }

      if ($response == "0") {
        // // redirect to confirmation
        $albumNa = preg_replace('/\-/', ' ', $albumName);
        $songsDownload = array();

        $srcDir = public_path('/storage/albums/' . $albumNa);
        $zipFileName = $albumName . '.zip';
        $files = scandir($srcDir);

        foreach ($files as $file) {
          $ext = pathinfo($file, PATHINFO_EXTENSION);
          if ($ext == 'mp3') {
            array_push($songsDownload, $file);
          }
        }

        $zip = new ZipArchive;
        $openZip = $zip->open(
          $srcDir . '/' . $zipFileName,
          ZipArchive::CREATE | ZipArchive::OVERWRITE
        );
        if ($openZip === TRUE) {
          foreach ($songsDownload as $song) {
            $zip->addFile($srcDir . '/' . $song);
          }
          $zip->close();
        }
        $filetopath = $srcDir . '/' . $zipFileName;

        if (file_exists($filetopath)) {
          return response()->download($filetopath)->deleteFileAfterSend(true);
        }
        return view('album/show', compact('album'));

        return redirect()->route('/album');
      } else {
        // stay on current paage
        // display error message
        echo 'user has a pending transaction, kindly try again in a few';
      }
    }
    // return redirect()->route('/album');
    // return redirect()->route('/storage/albums/' . $albumName);
    return view('album/show', compact('album'));
  }
}
